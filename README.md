### Master Spreadsheet git branching tutorial

Demo for modifying the master spreadsheet with git branching.

Steps:

1. Go to project repository on Gitlab and create a new branch. Give it a simple name, no spaces.
2. Open Git Bash on your machine. If using it for the first time, configure your information:

    ```git config --global user.name "myusername"```
    
    ```git config --global user.email "myemail@email.com"```

3. In git bash, cd to where you want to clone your new branch, e.g. ```cd "desktop"```
4. ```git clone -b <branch name> <git address>```

e.g.:

```git clone -b namefix2020 https://gitlab.com/geopig/branch-tutorial.git```

5. cd to the folder that is created, e.g. ```cd "branch-tutorial"```
6. make changes to the files inside of the folder using Excel, etc.
7. ```git add .```
8. ```git commit -m "my message about what I changed"```
9. ```git push origin <branch name>```

e.g.:

```git push origin namefix2020```

10. Go to Gitlab and create a merge request.